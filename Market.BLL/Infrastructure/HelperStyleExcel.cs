﻿using OfficeOpenXml;
using OfficeOpenXml.Style;
using System.Drawing;

namespace Market.BLL.Infrastructure
{
    public static class HelperStyleExcel
    {
        public static void BorderStyle(ExcelRange range, Color color)
        {
            using (range)
            {
                range.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                range.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                range.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                range.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                range.Style.Border.Top.Color.SetColor(color);
                range.Style.Border.Left.Color.SetColor(color);
                range.Style.Border.Right.Color.SetColor(color);
                range.Style.Border.Bottom.Color.SetColor(color);
            }
        }

        public static void AlignmentAndBackgroundColor(ExcelRange range, Color color)
        {
            using (range)
            {
                range.Style.Font.Bold = true;
                range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                range.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                range.Style.Fill.PatternType = ExcelFillStyle.Solid;
                range.Style.Fill.BackgroundColor.SetColor(color);
            }
        }
    }
}
