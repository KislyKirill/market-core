﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Market.BLL.Infrastructure
{
    public class ResultOperation
    {
        public bool IsSuccess { get; set; }
        public string Message { get; set; }
        public object Property { get; set; }
    }
}
