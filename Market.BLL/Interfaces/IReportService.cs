﻿using Market.BLL.DTO;
using System;
using System.IO;

namespace Market.BLL.Interfaces
{
    public interface IReportService
    {
        MemoryStream ExportExcel(OrderListDTO orderListDto, DateTime currentDate);
    }
}
