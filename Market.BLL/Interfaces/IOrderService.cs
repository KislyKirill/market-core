﻿using Market.BLL.Infrastructure;
using Market.Models.Entities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace Market.BLL.Interfaces
{
    public interface IOrderService
    {
        void MakeOrder(Order order, string userId);
        IEnumerable<Order> GetOrders();
        Order GetOrder(int? id);
        IEnumerable<Order> GetOrdersCurrentUser(string userId);
        IEnumerable<Order> Report(string userData, DateTime? fromDate, DateTime? toDate);
        Task<ResultOperation> ExportToExcel(IEnumerable<Order> orders, DateTime? fromDate, DateTime? toDate, string userData);
        void Dispose();
    }
}
