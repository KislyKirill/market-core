﻿using Market.BLL.DTO;
using Market.BLL.Infrastructure;
using Market.Models.Entities;
using Microsoft.AspNetCore.Authentication;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Market.BLL.Interfaces
{
    public interface IUserService : IDisposable
    {
        Task<User> GetUser(string id);
        IEnumerable<User> GetUsers();
        Task<List<string>> GetUserRoles(User user);
        Task<ResultOperation> ConfirmEmail(string userId, string code);
        Task<ResultOperation> Register(UserDTO userDto, string host);
        Task<ResultOperation> Login(UserDTO userDto);
        AuthenticationProperties SignInWithGoogle(string redirectUrl);
        Task<ResultOperation> HandleExternalLogin();
        Task<ResultOperation> LogOut();
    }
}
