﻿using Market.Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Market.BLL.Interfaces
{
    public interface IUserProfileService
    {
        UserProfile GetUserProfile(string id);
        IEnumerable<UserProfile> GetUserProfiles();
        void Create(UserProfile userProfile);
        void Update(UserProfile userProfile);
        void Delete(int? id);
        void Dispose();
    }
}
