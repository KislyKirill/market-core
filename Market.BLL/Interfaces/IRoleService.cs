﻿using Market.BLL.Infrastructure;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Market.BLL.Interfaces
{
    public interface IRoleService : IDisposable
    {
        List<IdentityRole> GetRoles();
        Task<ResultOperation> Create(string name);
        Task<ResultOperation> Edit(string userId, List<string> roles);
        Task<ResultOperation> Delete(string id);
    }
}
