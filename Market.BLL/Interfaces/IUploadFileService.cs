﻿using Market.Models;
using Market.Models.Entities;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Market.BLL.Interfaces
{
    public interface IUploadFileService
    {
        Task AddFileUser(IFormFile uploadedFile, string path, string name, UserProfile userProfile);
        Task AddFileProduct(IFormFileCollection uploads, Product product);
        FileModel GetFile(int? id);
        void Delete(int? id);
        void Dispose();
    }
}
