﻿using Market.Models.Entities;
using System.Collections.Generic;

namespace Market.BLL.Interfaces
{
    public interface IProductService
    {
        Product GetProduct(int? id);
        IEnumerable<Product> GetProducts();
        void Create(Product product);
        void Update(Product product);
        void Delete(int? id);
        void Dispose();
    }
}
