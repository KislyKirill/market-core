﻿using MimeKit;
using MailKit.Net.Smtp;
using System.Threading.Tasks;
using Market.BLL.Interfaces;
using Microsoft.Extensions.Options;
using Market.Models;

namespace Market.BLL.Services
{
    public class EmailService : IEmailService
    {
        public EmailSettings _emailSettings { get; set; }

        public EmailService(IOptions<EmailSettings> emailSettings)
        {
            _emailSettings = emailSettings.Value;
        }

        public async Task SendEmailAsync(string email, string subject, string message, string attachmentPath)
        {
            var emailMessage = new MimeMessage();

            emailMessage.From.Add(new MailboxAddress("Site administration", _emailSettings.UserNameEmail));
            emailMessage.To.Add(new MailboxAddress("", email));
            emailMessage.Subject = subject;

            var builder = new BodyBuilder();
            builder.TextBody = message;

            if (!string.IsNullOrEmpty(attachmentPath))
            {
                builder.Attachments.Add(attachmentPath);
            }

            emailMessage.Body = builder.ToMessageBody(); 

            using (var client = new SmtpClient())
            {
                await client.ConnectAsync(_emailSettings.Domain, _emailSettings.Port, false);
                await client.AuthenticateAsync(_emailSettings.UserNameEmail, _emailSettings.userNamePassword);
                await client.SendAsync(emailMessage);

                await client.DisconnectAsync(true);
            }
        }
    }
}
