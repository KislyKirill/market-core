﻿using Market.BLL.Infrastructure;
using Market.BLL.Interfaces;
using Market.DAL.Interfaces;
using Market.Models.Entities;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Market.BLL.Services
{
    public class RoleService : IRoleService
    {
        IUnitOfWork Database { get; set; }

        private readonly UserManager<User> _userManager;
        private readonly RoleManager<IdentityRole> _roleManager;

        public RoleService(IUnitOfWork uow, UserManager<User> userManager, RoleManager<IdentityRole> roleManager)
        {
            Database = uow;

            _userManager = userManager;
            _roleManager = roleManager;
        }

        public List<IdentityRole> GetRoles()
        {
            var allRoles = _roleManager.Roles.ToList();

            return allRoles;
        }

        public async Task<ResultOperation> Create(string name)
        {
            var result = new ResultOperation();

            if (!string.IsNullOrEmpty(name))
            {
                var identityResult = await _roleManager.CreateAsync(new IdentityRole(name));

                result.IsSuccess = identityResult.Succeeded;

                if (!result.IsSuccess)
                {
                    // Get last error 
                    var errors = identityResult.Errors.Last();
                    result.Message = errors.Description;
                }
            }

            return result;
        }

        public async Task<ResultOperation> Edit(string userId, List<string> roles)
        {
            User user = await _userManager.FindByIdAsync(userId);
            var result = new ResultOperation();

            if (user != null)
            {
                var userRoles = await _userManager.GetRolesAsync(user);
                var allRoles = _roleManager.Roles.ToList();
                var addedRoles = roles.Except(userRoles);
                var removedRoles = userRoles.Except(roles);
                await _userManager.AddToRolesAsync(user, addedRoles);
                await _userManager.RemoveFromRolesAsync(user, removedRoles);

                result.IsSuccess = true;
            }
            else
                result.IsSuccess = false;

            return result;
        }

        public async Task<ResultOperation> Delete(string id)
        {
            var role = await _roleManager.FindByIdAsync(id);

            if (role != null)
            {
                await _roleManager.DeleteAsync(role);
            }

            return new ResultOperation();
        }

        public void Dispose()
        {
            Database.Dispose();
        }
    }
}
