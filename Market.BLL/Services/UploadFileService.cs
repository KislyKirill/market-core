﻿using Market.BLL.Infrastructure;
using Market.BLL.Interfaces;
using Market.DAL.Interfaces;
using Market.Models;
using Market.Models.Entities;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace Market.BLL.Services
{
    public class UploadFileService : IUploadFileService
    {
        IUnitOfWork Database { get; set; }
        private IHostingEnvironment _hostingEnvironment;

        public UploadFileService(IUnitOfWork uow, IHostingEnvironment hostingEnv)
        {
            Database = uow;
            _hostingEnvironment = hostingEnv;
        }

        public async Task AddFileUser(IFormFile uploadedFile, string path, string name, UserProfile userProfile)
        {
            if (uploadedFile != null && path != null && name != null)
            {
                string fullPath = _hostingEnvironment.WebRootPath + path;
                if (!Directory.Exists(fullPath))
                {
                    Directory.CreateDirectory(fullPath);
                }

                var fileExt = "." + Path.GetExtension(uploadedFile.FileName).Substring(1);
                var nameWithExt = name + fileExt;

                using (var fileStream = new FileStream(fullPath + "/" + nameWithExt, FileMode.Create))
                {
                    await uploadedFile.CopyToAsync(fileStream);
                }

                FileModel file = new FileModel { Name = nameWithExt, Path = path };
                userProfile.FileModel = file;

                Database.Files.Create(file);
                Database.Save();

                Database.UserProfiles.Update(userProfile);
                Database.Save();
            }
        }

        public async Task AddFileProduct(IFormFileCollection uploads, Product product)
        {
            if (uploads != null || uploads.Count != 0)
            {
                product.Photos = new List<FileModel>();

                var path = "/images/products/" + product.ProductName;
                var pathSave = _hostingEnvironment.WebRootPath + path;
                if (!Directory.Exists(pathSave))
                {
                    Directory.CreateDirectory(pathSave);
                }

                foreach (var photo in uploads)
                {
                    using (var fileStream = new FileStream(pathSave + "/" + photo.FileName, FileMode.Create))
                    {
                        await photo.CopyToAsync(fileStream);
                    }

                    FileModel file = new FileModel { Name = photo.FileName, Path = path };

                    product.Photos.Add(file);
                }
            }

            Database.Save();
        }

        public FileModel GetFile(int? id)
        {
            if (id == null)
                throw new ValidationException("File id not set", "");

            var file = Database.Files.Get(id.Value);
            if (file == null)
                throw new ValidationException("File not found", "");

            return file;
        }

        public void Delete(int? id)
        {
            if (id == null)
                throw new ValidationException("Image id not set", "");

            Database.Files.Delete(id.Value);
            Database.Save();
        }

        public void Dispose()
        {
            Database.Dispose();
        }
    }
}
