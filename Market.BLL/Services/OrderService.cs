﻿using AutoMapper;
using Market.BLL.Infrastructure;
using Market.BLL.Interfaces;
using Market.DAL.Interfaces;
using Market.Models.Entities;
using Microsoft.AspNetCore.Hosting;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Market.BLL.Services
{
    public class OrderService : IOrderService
    {
        IUnitOfWork Database { get; set; }
        Cart cart;
        private IHostingEnvironment _hostingEnvironment;

        public OrderService(IUnitOfWork uow, Cart cartServ, IHostingEnvironment hostingEnv)
        {
            Database = uow;
            cart = cartServ;
            _hostingEnvironment = hostingEnv;
        }

        public void MakeOrder(Order order, string userId)
        {
            if (cart.Lines.Count() == 0)
                throw new ValidationException("There are no products in the order", "");

            order.UserId = userId;
            Database.Orders.Create(order);
            Database.Save();

            foreach (var item in cart.Lines)
            {
                var cartLine = new CartLine
                {
                    OrderId = order.OrderId,
                    ProductId = item.Product.ProductId,
                    Quantity = item.Quantity
                };

                Database.CartLines.Create(cartLine);
                Database.Save();
            }
        }

        public IEnumerable<Order> GetOrders()
        {
            return Database.Orders.GetAll();
        }

        public IEnumerable<Order> GetOrdersCurrentUser(string userId)
        {
            var allOrder = Database.Orders.GetAll();
            var ordersUser = allOrder.Where(p => p.UserId == userId);

            return ordersUser;
        }

        public Order GetOrder(int? id)
        {
            if (id == null)
                throw new ValidationException("Order id not set", "");

            var order = Database.Orders.Get(id.Value);
            if (order == null)
                throw new ValidationException("The order is not found", "");

            return order;
        }

        public IEnumerable<Order> Report(string userData, DateTime? fromDate, DateTime? toDate)
        {
            var orders = Database.Orders.GetAll();

            if (fromDate != null && toDate != null && fromDate <= toDate)
            {
                orders = orders.Where(p => p.PurchaseDate >= fromDate && p.PurchaseDate <= toDate);
            }

            if (!string.IsNullOrEmpty(userData))
            {
                orders = orders.Where(p => p.User.Id.Contains(userData) || p.User.Email.Contains(userData));
            }

            //if (fromDate != null && toDate != null && fromDate <= toDate && userData == null)
            //{
            //    orders = orders.Where(o => o.PurchaseDate >= fromDate && o.PurchaseDate <= toDate);
            //}
            //if (userData != null && (fromDate != null && toDate != null && fromDate <= toDate))
            //{
            //    orders = orders.Where(o => (o.PurchaseDate >= fromDate && o.PurchaseDate <= toDate)
            //                          && (o.User.Id == userData || o.User.Email == userData));
            //}
            //else if (userData != null)
            //{
            //    orders = orders.Where(u => u.User.Id == userData || u.User.Email == userData);
            //}

            return orders;
        }

        public async Task<ResultOperation> ExportToExcel(IEnumerable<Order> orders, DateTime? fromDate, DateTime? toDate, string userData)
        {
            DateTime currentDate = DateTime.Now;
            string DateName = currentDate.ToShortDateString();

            string sWebRootFolder = _hostingEnvironment.WebRootPath;
            string sFileName = $"{DateName}-report.xlsx";
            //string URL = string.Format("{0}://{1}/{2}", Request.Scheme, Request.Host, sFileName);
            FileInfo file = new FileInfo(Path.Combine(sWebRootFolder, sFileName));
            var memory = new MemoryStream();
            using (var fs = new FileStream(Path.Combine(sWebRootFolder, sFileName), FileMode.Create, FileAccess.Write))
            {
                IWorkbook workbook;
                workbook = new XSSFWorkbook();
                ISheet excelSheet = workbook.CreateSheet("Report");

                IRow row = excelSheet.CreateRow(0);
                row.CreateCell(0).SetCellValue("Date:");
                row.CreateCell(1).SetCellValue(currentDate.ToShortDateString());

                row = excelSheet.CreateRow(1);
                row.CreateCell(0).SetCellValue("Time:");
                row.CreateCell(1).SetCellValue(currentDate.ToLongTimeString());

                row = excelSheet.CreateRow(4);
                row.CreateCell(0).SetCellValue("Filter condition");

                row = excelSheet.CreateRow(5);
                row.CreateCell(0).SetCellValue("From date");
                row.CreateCell(1).SetCellValue("To date");
                row.CreateCell(2).SetCellValue("User ID (or email)");

                row = excelSheet.CreateRow(6);
                if (fromDate != null)
                    row.CreateCell(0).SetCellValue(fromDate.Value);
                if (toDate != null)
                    row.CreateCell(1).SetCellValue(toDate.Value);
                row.CreateCell(2).SetCellValue(userData);

                row = excelSheet.CreateRow(9);
                row.CreateCell(0).SetCellValue("Result");

                row = excelSheet.CreateRow(10);
                row.CreateCell(0).SetCellValue("Order ID");
                row.CreateCell(1).SetCellValue("User ID");
                row.CreateCell(2).SetCellValue("Email address");
                row.CreateCell(3).SetCellValue("Purchase date");
                row.CreateCell(4).SetCellValue("Product");
                row.CreateCell(5).SetCellValue("Count");
                row.CreateCell(6).SetCellValue("Amount");

                int rowCount = 11;
                row = excelSheet.CreateRow(rowCount);
                foreach (var item in orders)
                {
                    row.CreateCell(0).SetCellValue(item.OrderId);
                    row.CreateCell(1).SetCellValue(item.UserId);
                    row.CreateCell(2).SetCellValue(item.User.Email);
                    row.CreateCell(3).SetCellValue(item.PurchaseDate.ToShortDateString());

                    int totalPrice = 0;
                    foreach (var itemProduct in item.Lines)
                    {
                        row.CreateCell(4).SetCellValue(itemProduct.Product.ProductName);
                        row.CreateCell(5).SetCellValue(itemProduct.Quantity);

                        totalPrice += itemProduct.Product.Price * itemProduct.Quantity;
                        row = excelSheet.CreateRow(++rowCount);
                    }
                    row.CreateCell(6).SetCellValue(totalPrice);
                    row = excelSheet.CreateRow(++rowCount);
                }

                workbook.Write(fs);
            }
            using (var stream = new FileStream(Path.Combine(sWebRootFolder, sFileName), FileMode.Open))
            {
                await stream.CopyToAsync(memory);
            }
            memory.Position = 0;

            //File(memory, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", sFileName);

            var result = new ResultOperation();

            result.IsSuccess = true;

            return result;
        }

        public void Dispose()
        {
            Database.Dispose();
        }
    }
}
