﻿using Market.BLL.DTO;
using Market.BLL.Infrastructure;
using Market.BLL.Interfaces;
using Market.Models.Entities;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Text;

namespace Market.BLL.Services
{
    public class ReportService : IReportService
    {
        public MemoryStream ExportExcel(OrderListDTO orderListDto, DateTime currentDate)
        {
            var stream = new MemoryStream();

            using (var excel = new ExcelPackage(stream))
            {
                //ExcelPackage excel = new ExcelPackage();
                var workSheet = excel.Workbook.Worksheets.Add("Sheet1");
                workSheet.TabColor = System.Drawing.Color.Black;
                workSheet.DefaultRowHeight = 12;
                workSheet.DefaultColWidth = 23;

                // Report creation (header)
                workSheet.Row(1).Height = 20;

                workSheet.Cells["A1"].Value = "Report creation";
                workSheet.Cells["A1:B1"].Merge = true;

                // Report creation (body)
                workSheet.Row(2).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                workSheet.Cells["A2"].Value = "Date:";
                workSheet.Cells["B2"].Value = "Time:";
                workSheet.Cells["A3"].Value = string.Format("{0:yyyy/MM/dd}", currentDate);
                workSheet.Cells["B3"].Value = string.Format("{0:HH:mm:ss.fff}", currentDate);

                // Filter condition (header)
                workSheet.Row(6).Height = 20;
                workSheet.Row(6).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                workSheet.Row(6).Style.Font.Bold = true;
                workSheet.Cells["A6"].Value = "Filter condition";
                workSheet.Cells["A6:C6"].Merge = true;

                // Filter condition (body)
                workSheet.Row(7).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                workSheet.Cells["A7"].Value = "From date:";
                workSheet.Cells["B7"].Value = "To date:";
                workSheet.Cells["C7"].Value = "User ID (or email):";
                if (orderListDto.FromDate != null)
                    workSheet.Cells["A8"].Value = orderListDto.FromDate.Value.ToShortDateString();
                if (orderListDto.ToDate != null)
                    workSheet.Cells["B8"].Value = orderListDto.ToDate.Value.ToShortDateString();
                workSheet.Cells["C8"].Value = orderListDto.UserData;

                // Header of table
                workSheet.Row(11).Height = 20;
                workSheet.Row(11).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                workSheet.Row(11).Style.Font.Bold = true;
                workSheet.Cells["B11"].Value = "Order ID";
                workSheet.Cells["C11"].Value = "User ID";
                workSheet.Cells["D11"].Value = "Email address";
                workSheet.Cells["E11"].Value = "Purchase date";
                workSheet.Cells["F11"].Value = "Product";
                workSheet.Cells["G11"].Value = "Count";
                workSheet.Cells["H11"].Value = "Amount";

                int recordIndex = 12;
                foreach (var item in orderListDto.Orders)
                {
                    workSheet.Cells[recordIndex, 2].Value = item.OrderId;
                    workSheet.Cells[recordIndex, 3].Value = item.UserId;
                    workSheet.Cells[recordIndex, 4].Value = item.User.Email;
                    workSheet.Cells[recordIndex, 5].Value = item.PurchaseDate.ToShortDateString();

                    int totalPrice = 0;
                    foreach (var itemProduct in item.Lines)
                    {
                        workSheet.Cells[recordIndex, 6].Value = itemProduct.Product.ProductName;
                        workSheet.Cells[recordIndex, 7].Value = itemProduct.Quantity;

                        totalPrice += itemProduct.Product.Price * itemProduct.Quantity;
                        recordIndex++;
                    }
                    workSheet.Cells[recordIndex, 8].Value = totalPrice;

                    recordIndex++;
                }

                workSheet.Column(1).AutoFit();
                workSheet.Column(2).AutoFit();
                workSheet.Column(3).AutoFit();
                workSheet.Column(4).AutoFit();
                workSheet.Column(5).AutoFit();
                workSheet.Column(6).AutoFit();
                workSheet.Column(7).AutoFit();
                workSheet.Column(8).AutoFit();

                HelperStyleExcel.BorderStyle(workSheet.Cells["A1:B3"], Color.DarkGray);
                HelperStyleExcel.AlignmentAndBackgroundColor(workSheet.Cells["A1:B1"], Color.LightGray);

                HelperStyleExcel.BorderStyle(workSheet.Cells["A6:C8"], Color.DarkGray);
                HelperStyleExcel.AlignmentAndBackgroundColor(workSheet.Cells["A6:C6"], Color.LightGray);

                HelperStyleExcel.BorderStyle(workSheet.Cells["B11:H" + (recordIndex - 1)], Color.DarkGray);
                HelperStyleExcel.AlignmentAndBackgroundColor(workSheet.Cells["B11:H11"], Color.LightGray);

                excel.Save();
            }

            stream.Position = 0;

            return stream;
        }
    }
}
