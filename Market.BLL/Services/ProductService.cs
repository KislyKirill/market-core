﻿using Market.BLL.Infrastructure;
using Market.BLL.Interfaces;
using Market.DAL.Interfaces;
using Market.Models.Entities;
using System;
using System.Collections.Generic;

namespace Market.BLL.Services
{
    public class ProductService : IProductService
    {
        IUnitOfWork Database { get; set; }

        public ProductService(IUnitOfWork uow)
        {
            Database = uow;
        }

        public Product GetProduct(int? id)
        {
            if (id == null)
                throw new ValidationException("Product id not set", "");

            var product = Database.Products.Get(id.Value);
            if (product == null)
                throw new ValidationException("Product not found", "");

            return product;
        }

        public IEnumerable<Product> GetProducts()
        {
            return Database.Products.GetAll();
        }

        public void Create(Product product)
        {
            Database.Products.Create(product);
            Database.Save();
        }

        public void Update(Product product)
        {
            Database.Products.Update(product);
            Database.Save();
        }

        public void Delete(int? id)
        {
            if (id == null)
                throw new ValidationException("Product id not set", "");

            Database.Products.Delete(id.Value);
            Database.Save();
        }

        public void Dispose()
        {
            Database.Dispose();
        }
    }
}
