﻿using Market.BLL.DTO;
using Market.BLL.Infrastructure;
using Market.BLL.Interfaces;
using Market.DAL.Interfaces;
using Market.Models;
using Market.Models.Entities;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Market.BLL.Services
{
    public class UserService : IUserService
    {
        IUnitOfWork Database { get; set; }

        private readonly UserManager<User> _userManager;
        private readonly SignInManager<User> _signInManager;
        private readonly IEmailService _emailService;

        public UserService(IUnitOfWork uow, UserManager<User> userManager, SignInManager<User> signInManager, IEmailService emailService)
        {
            Database = uow;

            _userManager = userManager;
            _signInManager = signInManager;
            _emailService = emailService;
        }

        public async Task<User> GetUser(string id)
        {
            User user = await _userManager.FindByIdAsync(id);
            return user;
        }

        public IEnumerable<User> GetUsers()
        {
            var allUsers = _userManager.Users.ToList();
            return allUsers;
        }

        public async Task<List<string>> GetUserRoles(User user)
        {
            var userRoles = await _userManager.GetRolesAsync(user);
            return userRoles.ToList();
        }

        public async Task<ResultOperation> Register(UserDTO userDto, string host)
        {
            var user = new User { Email = userDto.Email, UserName = userDto.Email };
            var identityResult = await _userManager.CreateAsync(user, userDto.Password);

            var result = new ResultOperation()
            {
                IsSuccess = identityResult.Succeeded
            };

            if (result.IsSuccess)
            {
                // Static null avatar
                FileModel fileModel = new FileModel
                {
                    Name = "http://ssl.gstatic.com/accounts/ui/avatar_2x.png"
                };

                UserProfile userProfile = new UserProfile
                {
                    Id = user.Id,
                    FirstName = userDto.FirstName,
                    LastName = userDto.LastName,
                    RegistrationDate = userDto.RegistrationDate,
                    Email = user.Email,
                    FileModel = fileModel
                };
                Database.UserProfiles.Create(userProfile);
                await Database.SaveAsync();

                // Get user token
                var code = await _userManager.GenerateEmailConfirmationTokenAsync(user);
                var encode = HttpUtility.UrlEncode(code);

                // Generate callbackUrl
                var callbackUrl = new StringBuilder();
                callbackUrl.AppendFormat(host)
                    .AppendFormat("/Account/ConfirmEmail")
                    .AppendFormat("?userId=")
                    .AppendFormat(user.Id)
                    .AppendFormat("&code=")
                    .AppendFormat(encode);

                // Send mail
                //EmailService emailService = new EmailService();
                await _emailService.SendEmailAsync(user.Email, "Account verification", $"{userProfile.FirstName} {userProfile.LastName}, " +
                    $"You are welcomed by the administration of the market product website. Confirm your registration by clicking on the link: <a href='{callbackUrl}'>link</a>", null);

                result.Message = "To complete the registration, check your email and follow the link provided in the email";
            }
            else
            {
                // Get last error 
                var errors = identityResult.Errors.Last();
                result.Message = errors.Description;
            }

            return result;
        }

        public async Task<ResultOperation> ConfirmEmail(string userId, string code)
        {
            var resultOperation = new ResultOperation();

            if (userId == null || code == null)
            {
                resultOperation.Message = "User id not set or link incorrect";
            }

            var user = await _userManager.FindByIdAsync(userId);
            if (user == null)
            {
                resultOperation.Message = "User not found";
            }

            var result = await _userManager.ConfirmEmailAsync(user, code);
            if (result.Succeeded)
            {
                await _userManager.AddToRoleAsync(user, "user");
                await _signInManager.SignInAsync(user, false);
            }
            else
            {
                var errors = result.Errors.Last();
                resultOperation.Message = errors.Description;
            }
            resultOperation.IsSuccess = result.Succeeded;

            return resultOperation;
        }

        public async Task<ResultOperation> Login(UserDTO userDto)
        {
            var result = new ResultOperation();

            var getUser = await _userManager.FindByNameAsync(userDto.Email);
            if (getUser != null)
            {
                if (!await _userManager.IsEmailConfirmedAsync(getUser))
                {
                    result.Message = "You have not confirmed your email";
                }
            }

            var identityResult = await _signInManager.PasswordSignInAsync(userDto.Email, userDto.Password, true, false);
            if (!identityResult.Succeeded)
            {
                result.Message = "Invalid login or password";
            }

            result.IsSuccess = identityResult.Succeeded;

            return result;
        }

        public AuthenticationProperties SignInWithGoogle(string redirectUrl)
        {
            var authenticationProperties = _signInManager.ConfigureExternalAuthenticationProperties("Google", redirectUrl);
            return authenticationProperties;
        }

        public async Task<ResultOperation> HandleExternalLogin()
        {
            var resultOperation = new ResultOperation();
            var info = await _signInManager.GetExternalLoginInfoAsync();
            var result = await _signInManager.ExternalLoginSignInAsync(info.LoginProvider, info.ProviderKey, isPersistent: false);

            if (!result.Succeeded) //user does not exist yet
            {
                var email = info.Principal.FindFirstValue(ClaimTypes.Email);
                var newUser = new User
                {
                    UserName = email,
                    Email = email,
                    EmailConfirmed = true
                };
                var createResult = await _userManager.CreateAsync(newUser);

                resultOperation.IsSuccess = createResult.Succeeded;

                if (!createResult.Succeeded)
                {
                    var errors = createResult.Errors.Last();
                    resultOperation.Message = errors.Description;
                }
                await _userManager.AddToRoleAsync(newUser, "user");
                await _userManager.AddLoginAsync(newUser, info);

                // Static null avatar
                FileModel fileModel = new FileModel
                {
                    Name = "http://ssl.gstatic.com/accounts/ui/avatar_2x.png"
                };

                UserProfile userProfile = new UserProfile
                {
                    Id = newUser.Id,
                    Email = email,
                    RegistrationDate = DateTime.Now,
                    FileModel = fileModel
                };
                Database.UserProfiles.Create(userProfile);
                await Database.SaveAsync();

                var newUserClaims = info.Principal.Claims.Append(new Claim("userId", newUser.Id));
                await _userManager.AddClaimsAsync(newUser, newUserClaims);
                await _signInManager.SignInAsync(newUser, isPersistent: false);
            }
            else
            {
                // Get exist user
                var user = await _userManager.FindByLoginAsync(info.LoginProvider, info.ProviderKey);

                await _userManager.AddClaimAsync(user, info.Principal.Claims.FirstOrDefault());
                await _signInManager.SignInAsync(user, isPersistent: false);

                resultOperation.IsSuccess = true;
            }

            return resultOperation;
        }

        public async Task<ResultOperation> LogOut()
        {
            await _signInManager.SignOutAsync();

            return new ResultOperation();
        }

        public void Dispose()
        {
            Database.Dispose();
        }
    }
}
