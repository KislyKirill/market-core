﻿using Market.BLL.Infrastructure;
using Market.BLL.Interfaces;
using Market.DAL.Interfaces;
using Market.Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Market.BLL.Services
{
    public class UserProfileService : IUserProfileService
    {
        IUnitOfWork Database { get; set; }

        public UserProfileService(IUnitOfWork uow)
        {
            Database = uow;
        }

        public UserProfile GetUserProfile(string id)
        {
            if (id == null)
                throw new ValidationException("User profile id not set", "");

            var userProfile = Database.UserProfiles.Get(id);
            if (userProfile == null)
                throw new ValidationException("User profile not found", "");

            return userProfile;
        }

        public IEnumerable<UserProfile> GetUserProfiles()
        {
            return Database.UserProfiles.GetAll();
        }

        public void Create(UserProfile userProfile)
        {
            Database.UserProfiles.Create(userProfile);
            Database.Save();
        }

        public void Update(UserProfile userProfile)
        {
            Database.UserProfiles.Update(userProfile);
            Database.Save();
        }

        public void Delete(int? id)
        {
            if (id == null)
                throw new ValidationException("User profile id not set", "");

            Database.UserProfiles.Delete(id.Value);
            Database.Save();
        }

        public void Dispose()
        {
            Database.Dispose();
        }
    }
}
