﻿using Market.Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Market.BLL.DTO
{
    public class OrderListDTO
    {
        public IEnumerable<Order> Orders { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public string UserData { get; set; }
    }
}
