﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Market.BLL.DTO
{
    public class UserDTO
    {
        public string id { get; set; }
        public string Email { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime RegistrationDate { get; set; }
        public int? ImageId { get; set; }
        public string ImagePath { get; set; }
        public string ImageName { get; set; }
    }
}
