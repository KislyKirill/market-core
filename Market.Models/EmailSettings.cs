﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Market.Models
{
    public class EmailSettings
    {
        public string Domain { get; set; }
        public int Port { get; set; }
        public string UserNameEmail { get; set; }
        public string userNamePassword { get; set; }
    }
}
