﻿using Market.Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Market.Models
{
    public class FileModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Path { get; set; }

        public virtual ICollection<User> Users { get; set; }
        public virtual Product Product { get; set; }
    }
}
