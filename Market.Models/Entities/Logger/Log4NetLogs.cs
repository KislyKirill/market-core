﻿
namespace Market.Models.Entities.Logger
{
    public class Log4NetLogs
    {
        public int Id { get; set; }
        // Request 
        public string RequestTime { get; set; }
        public string RequestUri { get; set; }
        public string Username { get; set; }
        public string Headers { get; set; }
        public string Body { get; set; }
        public string QueryString { get; set; }
        public string HttpVerb { get; set; }

        // Response
        public string ResponseTime { get; set; }
        public string StatusCode { get; set; }

        // Exception
        public string StackTrace { get; set; }
        public string MessageException { get; set; }
    }
}
