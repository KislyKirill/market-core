﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Market.Models.Entities
{
    public class Product
    {
        public int ProductId { get; set; }
        public string ProductName { get; set; }
        public int Price { get; set; }
        public string Description { get; set; }
        public DateTime AddedDate { get; set; }

        public virtual ICollection<CartLine> Lines { get; set; }

        [JsonIgnore]
        public virtual ICollection<FileModel> Photos { get; set; }
    }
}
