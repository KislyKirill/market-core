﻿using System;
using System.Collections.Generic;

namespace Market.Models.Entities
{
    public class Order
    {
        public int OrderId { get; set; }
        public DateTime PurchaseDate { get; set; }
        public string UserId { get; set; }

        public virtual User User { get; set; }
        public virtual ICollection<CartLine> Lines { get; set; }
    }
}
