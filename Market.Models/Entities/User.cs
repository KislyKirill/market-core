﻿using Microsoft.AspNetCore.Identity;
using System;

namespace Market.Models.Entities
{
    public class User : IdentityUser
    {
        public virtual UserProfile UserProfile { get; set; }
    }
}
