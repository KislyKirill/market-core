﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Market.DAL.Migrations
{
    public partial class removemessagefieldinlogger : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Message",
                table: "Logs");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Message",
                table: "Logs",
                nullable: true);
        }
    }
}
