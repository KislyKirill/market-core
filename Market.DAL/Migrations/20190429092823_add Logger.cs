﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Market.DAL.Migrations
{
    public partial class addLogger : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Logs",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    RequestTime = table.Column<string>(nullable: true),
                    RequestUri = table.Column<string>(nullable: true),
                    Username = table.Column<string>(nullable: true),
                    Headers = table.Column<string>(nullable: true),
                    Body = table.Column<string>(nullable: true),
                    QueryString = table.Column<string>(nullable: true),
                    HttpVerb = table.Column<string>(nullable: true),
                    ResponseTime = table.Column<string>(nullable: true),
                    StatusCode = table.Column<string>(nullable: true),
                    StackTrace = table.Column<string>(nullable: true),
                    MessageException = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Logs", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Logs");
        }
    }
}
