﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Market.DAL.Migrations
{
    public partial class Updateuserprofilefield : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_UserProfiles_Files_ImageId",
                table: "UserProfiles");

            migrationBuilder.RenameColumn(
                name: "ImageId",
                table: "UserProfiles",
                newName: "FileModelId");

            migrationBuilder.RenameIndex(
                name: "IX_UserProfiles_ImageId",
                table: "UserProfiles",
                newName: "IX_UserProfiles_FileModelId");

            migrationBuilder.AddForeignKey(
                name: "FK_UserProfiles_Files_FileModelId",
                table: "UserProfiles",
                column: "FileModelId",
                principalTable: "Files",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_UserProfiles_Files_FileModelId",
                table: "UserProfiles");

            migrationBuilder.RenameColumn(
                name: "FileModelId",
                table: "UserProfiles",
                newName: "ImageId");

            migrationBuilder.RenameIndex(
                name: "IX_UserProfiles_FileModelId",
                table: "UserProfiles",
                newName: "IX_UserProfiles_ImageId");

            migrationBuilder.AddForeignKey(
                name: "FK_UserProfiles_Files_ImageId",
                table: "UserProfiles",
                column: "ImageId",
                principalTable: "Files",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
