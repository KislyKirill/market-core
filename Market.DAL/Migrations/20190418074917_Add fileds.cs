﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Market.DAL.Migrations
{
    public partial class Addfileds : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Name",
                table: "AspNetUsers",
                newName: "PasswordConfirm");

            migrationBuilder.AddColumn<string>(
                name: "Password",
                table: "AspNetUsers",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Password",
                table: "AspNetUsers");

            migrationBuilder.RenameColumn(
                name: "PasswordConfirm",
                table: "AspNetUsers",
                newName: "Name");
        }
    }
}
