﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Market.DAL.Migrations
{
    public partial class Productimages : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "FileModelId",
                table: "Products",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Products_FileModelId",
                table: "Products",
                column: "FileModelId");

            migrationBuilder.AddForeignKey(
                name: "FK_Products_Files_FileModelId",
                table: "Products",
                column: "FileModelId",
                principalTable: "Files",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Products_Files_FileModelId",
                table: "Products");

            migrationBuilder.DropIndex(
                name: "IX_Products_FileModelId",
                table: "Products");

            migrationBuilder.DropColumn(
                name: "FileModelId",
                table: "Products");
        }
    }
}
