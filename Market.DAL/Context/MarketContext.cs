﻿using Market.Models;
using Market.Models.Entities;
using Market.Models.Entities.Logger;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace Market.DAL.Context
{
    public class MarketContext : IdentityDbContext<User>
    {
        public DbSet<Product> Products { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<CartLine> CartLines { get; set; }
        public DbSet<FileModel> Files { get; set; }
        public DbSet<UserProfile> UserProfiles { get; set; }

        public DbSet<Log4NetLogs> Logs { get; set; }

        public MarketContext(DbContextOptions<MarketContext> options) : base(options)
        {

        }
    }
}
