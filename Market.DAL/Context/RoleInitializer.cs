﻿using Market.DAL.Repositories;
using Market.Models.Entities;
using Microsoft.AspNetCore.Identity;
using System;
using System.Threading.Tasks;

namespace Market.DAL.Context
{
    public class RoleInitializer
    {
        public static async Task InitializeAsync(UserManager<User> userManager, RoleManager<IdentityRole> roleManager, MarketContext context)
        {
            string adminEmail = "admin@gmail.com";
            string password = "123Asd#";
            if (await roleManager.FindByNameAsync("admin") == null)
            {
                await roleManager.CreateAsync(new IdentityRole("admin"));
            }
            if (await roleManager.FindByNameAsync("user") == null)
            {
                await roleManager.CreateAsync(new IdentityRole("user"));
            }
            if (await userManager.FindByNameAsync(adminEmail) == null)
            {
                User admin = new User { Email = adminEmail, UserName = adminEmail, EmailConfirmed = true };
                IdentityResult result = await userManager.CreateAsync(admin, password);
                if (result.Succeeded)
                {
                    await userManager.AddToRoleAsync(admin, "admin");

                    UserProfile userProfile = new UserProfile
                    {
                        Id = admin.Id,
                        Email = admin.Email,
                        FirstName = admin.UserName,
                        LastName = admin.UserName,
                        RegistrationDate = DateTime.Now
                    };

                    context.UserProfiles.Add(userProfile);
                    context.SaveChanges();
                }
            }
        }
    }
}
