﻿using Market.Models;
using Market.Models.Entities;
using System;
using System.Threading.Tasks;

namespace Market.DAL.Interfaces
{
    public interface IUnitOfWork : IDisposable
    {
        IRepository<Order> Orders { get; }
        IRepository<Product> Products { get; }
        IRepository<CartLine> CartLines { get; }
        IRepository<FileModel> Files { get; }
        IUserProfileManager UserProfiles { get; }
        IUserManager UserManager { get; }
        IRoleManager RoleManager { get; }
        void Save();
        Task SaveAsync();
    }
}
