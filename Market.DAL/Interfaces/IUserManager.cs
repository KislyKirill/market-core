﻿using Market.Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Market.DAL.Interfaces
{
    public interface IUserManager : IDisposable
    {
        void Create(User item);
    }
}
