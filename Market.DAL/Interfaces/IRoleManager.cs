﻿using Market.Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Market.DAL.Interfaces
{
    public interface IRoleManager : IDisposable
    {
        void Create(Role item);
    }
}
