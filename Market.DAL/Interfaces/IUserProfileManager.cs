﻿using Market.Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Market.DAL.Interfaces
{
    public interface IUserProfileManager
    {
        IEnumerable<UserProfile> GetAll();
        UserProfile Get(string id);
        IEnumerable<UserProfile> Find(Func<UserProfile, bool> predicate);
        void Create(UserProfile item);
        void Update(UserProfile item);
        void Delete(int id);
    }
}
