﻿using Market.DAL.Context;
using Market.DAL.Interfaces;
using Market.Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Market.DAL.Repositories
{
    public class RoleRepository : IRoleManager
    {
        public MarketContext Database { get; set; }

        public RoleRepository(MarketContext db)
        {
            Database = db;
        }

        public void Create(Role item)
        {
            Database.Roles.Add(item);
            Database.SaveChanges();
        }

        public void Dispose()
        {
            Database.Dispose();
        }
    }
}
