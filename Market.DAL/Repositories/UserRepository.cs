﻿using Market.DAL.Context;
using Market.DAL.Interfaces;
using Market.Models.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace Market.DAL.Repositories
{
    public class UserRepository : IUserManager
    {
        public MarketContext Database { get; set; }

        public UserRepository(MarketContext db)
        {
            Database = db;
        }

        public void Create(User item)
        {
            Database.Users.Add(item);
            Database.SaveChanges();
        }

        public void Dispose()
        {
            Database.Dispose();
        }
    }
}
