﻿using Market.DAL.Context;
using Market.DAL.Interfaces;
using Market.Models.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Market.DAL.Repositories
{
    class CartLineRepository : IRepository<CartLine>
    {
        private MarketContext db;

        public CartLineRepository(MarketContext context)
        {
            this.db = context;
        }

        public IEnumerable<CartLine> GetAll()
        {
            return db.CartLines;
        }

        public CartLine Get(int id)
        {
            return db.CartLines.Find(id);
        }

        public void Create(CartLine cartLine)
        {
            db.CartLines.Add(cartLine);
        }

        public void Update(CartLine cartLine)
        {
            db.Entry(cartLine).State = EntityState.Modified;
        }

        public IEnumerable<CartLine> Find(Func<CartLine, bool> predicate)
        {
            return db.CartLines.Where(predicate).ToList();
        }

        public void Delete(int id)
        {
            CartLine cartLine = db.CartLines.Find(id);
            if (cartLine != null)
                db.CartLines.Remove(cartLine);
        }
    }
}
