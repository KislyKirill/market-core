﻿using Market.DAL.Context;
using Market.DAL.Interfaces;
using Market.Models.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Market.DAL.Repositories
{
    class UserProfileRepository : IUserProfileManager
    {
        private MarketContext db;

        public UserProfileRepository(MarketContext context)
        {
            this.db = context;
        }

        public IEnumerable<UserProfile> GetAll()
        {
            return db.UserProfiles;
        }

        public UserProfile Get(string id)
        {
            return db.UserProfiles.Find(id);
        }

        public void Create(UserProfile userProfile)
        {
            db.UserProfiles.Add(userProfile);
        }

        public void Update(UserProfile userProfile)
        {
            db.Entry(userProfile).State = EntityState.Modified;
        }

        public IEnumerable<UserProfile> Find(Func<UserProfile, bool> predicate)
        {
            return db.UserProfiles.Where(predicate).ToList();
        }

        public void Delete(int id)
        {
            UserProfile userProfile = db.UserProfiles.Find(id);
            if (userProfile != null)
                db.UserProfiles.Remove(userProfile);
        }
    }
}
