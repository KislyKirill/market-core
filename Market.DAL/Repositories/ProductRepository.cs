﻿using Market.DAL.Context;
using Market.DAL.Interfaces;
using Market.Models.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Market.DAL.Repositories
{
    class ProductRepository : IRepository<Product>
    {
        private MarketContext db;

        public ProductRepository(MarketContext context)
        {
            this.db = context;
        }

        public IEnumerable<Product> GetAll()
        {
            return db.Products.Include(p => p.Photos);
        }

        public Product Get(int id)
        {
            return db.Products.Include(p => p.Photos).SingleOrDefault(x => x.ProductId == id);
        }

        public void Create(Product product)
        {
            db.Products.Add(product);
        }

        public void Update(Product product)
        {
            db.Entry(product).State = EntityState.Modified;
        }

        public IEnumerable<Product> Find(Func<Product, bool> predicate)
        {
            return db.Products.Where(predicate).ToList();
        }

        public void Delete(int id)
        {
            Product product = db.Products.Include(p => p.Photos).SingleOrDefault(x => x.ProductId == id);
            if (product != null)
                db.Products.Remove(product);
        }
    }
}
