﻿using Market.DAL.Context;
using Market.DAL.Interfaces;
using Market.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Market.DAL.Repositories
{
    class FileRepository : IRepository<FileModel>
    {
        private MarketContext db;

        public FileRepository (MarketContext context)
        {
            this.db = context;
        }

        public void Create(FileModel fileModel)
        {
            db.Files.Add(fileModel);
        }

        public void Delete(int id)
        {
            FileModel fileModel = db.Files.Find(id);
            if (fileModel != null)
                db.Files.Remove(fileModel);
        }

        public IEnumerable<FileModel> Find(Func<FileModel, bool> predicate)
        {
            return db.Files.Where(predicate).ToList();
        }

        public FileModel Get(int id)
        {
            return db.Files.Find(id);
        }

        public IEnumerable<FileModel> GetAll()
        {
            throw new NotImplementedException();
        }

        public void Update(FileModel fileModel)
        {
            db.Entry(fileModel).State = EntityState.Modified;
        }
    }
}
