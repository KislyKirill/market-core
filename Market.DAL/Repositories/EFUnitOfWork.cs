﻿using Market.DAL.Context;
using Market.DAL.Interfaces;
using Market.Models;
using Market.Models.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Market.DAL.Repositories
{
    public class EFUnitOfWork : IUnitOfWork
    {
        private MarketContext db;

        private ProductRepository productRepository;
        private OrderRepository orderRepository;
        private UserRepository userRepository;
        private RoleRepository roleRepository;
        private CartLineRepository cartLineRepository;
        private FileRepository fileRepository;
        private UserProfileRepository userProfileRepository;


        public EFUnitOfWork(DbContextOptions<MarketContext> options)
        {
            db = new MarketContext(options);
            userRepository = new UserRepository(db);
            roleRepository = new RoleRepository(db);
            userProfileRepository = new UserProfileRepository(db);
        }

        public IRepository<Order> Orders {
            get
            {
                if (orderRepository == null)
                    orderRepository = new OrderRepository(db);

                return orderRepository;
            }
        }

        public IRepository<Product> Products {
            get
            {
                if (productRepository == null)
                    productRepository = new ProductRepository(db);

                return productRepository;
            }
        }

        public IRepository<CartLine> CartLines {
            get
            {
                if (cartLineRepository == null)
                    cartLineRepository = new CartLineRepository(db);

                return cartLineRepository;
            }
        }

        public IRepository<FileModel> Files {
            get
            {
                if (fileRepository == null)
                    fileRepository = new FileRepository(db);

                return fileRepository;
            }
        }

        public IUserProfileManager UserProfiles {
            get { return userProfileRepository; }
        }

        public IUserManager UserManager {
            get { return userRepository; }
        }

        public IRoleManager RoleManager {
            get { return roleRepository; }
        }

        public void Save()
        {
            db.SaveChanges();
        }

        public async Task SaveAsync()
        {
            await db.SaveChangesAsync();
        }

        private bool disposed = false;

        public virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    db.Dispose();
                    userRepository.Dispose();
                }
                this.disposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
