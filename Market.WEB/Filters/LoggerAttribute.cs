﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.AspNetCore.Http.Internal;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Market.WEB.Filters
{
    public class LoggerAttribute : Attribute, IActionFilter, IExceptionFilter
    {
        private readonly ILogger _logger;

        public LoggerAttribute(ILoggerFactory loggerFactory)
        {
            _logger = loggerFactory.CreateLogger("LoggerAttributeFilter");
        }

        public void OnActionExecuting(ActionExecutingContext context)
        {
            // Get informations
            var RequestTime = DateTime.Now.ToString("MM/dd/yyyy hh:mm:ss.fffff");
            var RequestUri = context.HttpContext.Request.GetDisplayUrl();
            var Username = context.HttpContext.User.Identity.Name ?? $"\"not an authorized user\"";
            var Headers = context.HttpContext.Request?.Headers;
            var Body = context.HttpContext.Request.Body;
            var QueryString = context.HttpContext.Request.QueryString;
            var HttpVerb = context.HttpContext.Request.Method;

            // Make log
            StringBuilder logExecuting = new StringBuilder();
            logExecuting
                .AppendFormat("\nRequest time: {0}\n", RequestTime)
                .AppendFormat("RequestUri: {0}\n", RequestUri)
                .AppendFormat("User name: {0}\n", Username)
                .AppendFormat("Headers: \n{0}\n", Headers)
                .AppendFormat("Body: \n{0}\n", Body)
                .AppendFormat("Query string: {0}\n", QueryString)
                .AppendFormat("HttpVerb: {0}\n", HttpVerb)
                .AppendFormat("--------------------------\n\n");

            // To database
            log4net.ThreadContext.Properties["RequestTime"] = RequestTime;
            log4net.ThreadContext.Properties["RequestUri"] = RequestUri;
            log4net.ThreadContext.Properties["Username"] = Username;
            log4net.ThreadContext.Properties["Headers"] = Headers;
            log4net.ThreadContext.Properties["Body"] = Body;
            log4net.ThreadContext.Properties["QueryString"] = QueryString;
            log4net.ThreadContext.Properties["HttpVerb"] = HttpVerb;

            _logger.LogInformation(logExecuting.ToString());
        }

        public void OnActionExecuted(ActionExecutedContext context)
        {
            // Get informations
            var ResponseTime = DateTime.Now.ToString("MM/dd/yyyy hh:mm:ss.fffff");
            var RequestUri = context.HttpContext.Request.GetDisplayUrl();
            var Username = context.HttpContext.User.Identity.Name;
            var Headers = context.HttpContext.Request.Headers;
            var Body = context.HttpContext.Request.Body;
            var StatusCode = context.HttpContext.Response.StatusCode;

            // Make log
            StringBuilder logExecuted = new StringBuilder();
            logExecuted
                .AppendFormat("\nResponse time: {0}\n", ResponseTime)
                .AppendFormat("RequestUri: {0}\n", RequestUri)
                .AppendFormat("User name: {0}\n", Username)
                .AppendFormat("Headers: \n{0}\n", Headers)
                .AppendFormat("Body: \n{0}\n", Body)
                .AppendFormat("Status code: {0}\n", StatusCode)
                .AppendFormat("--------------------------\n\n");

            // To database
            log4net.ThreadContext.Properties["ResponseTime"] = ResponseTime;
            log4net.ThreadContext.Properties["RequestUri"] = RequestUri;
            log4net.ThreadContext.Properties["Username"] = Username;
            log4net.ThreadContext.Properties["Headers"] = Headers;
            log4net.ThreadContext.Properties["Body"] = Body;
            log4net.ThreadContext.Properties["StatusCode"] = StatusCode;

            _logger.LogInformation(logExecuted.ToString());
        }

        public void OnException(ExceptionContext context)
        {
            // Get informations
            var RequestUri = context.HttpContext.Request.GetDisplayUrl();
            var Username = context.HttpContext.User.Identity.Name;
            var Headers = context.HttpContext.Request.Headers;
            var Body = context.HttpContext.Request.Body;
            var QueryString = context.HttpContext.Request.QueryString;
            var HttpVerb = context.HttpContext.Request.Method;

            var StatusCode = context.HttpContext.Response.StatusCode;
            var StackTrace = context.Exception.StackTrace;
            var MessageException = context.Exception.Message;

            // Make log
            StringBuilder logException = new StringBuilder();
            logException
                .AppendFormat("\n\n---------------------------\n")
                .AppendFormat("|         Exceptions         |")
                .AppendFormat("\n---------------------------\n")
                .AppendFormat("RequestUri: {0}\n", RequestUri)
                .AppendFormat("User name: {0}\n", Username)
                .AppendFormat("Headers: \n{0}\n", Headers)
                .AppendFormat("Body: \n{0}\n", Body)
                .AppendFormat("Query string: {0}\n", QueryString)
                .AppendFormat("Http verb: {0}\n", HttpVerb)
                .AppendFormat("Status code: {0}\n", StatusCode)
                .AppendFormat("Stack trace: {0}\n", StackTrace)
                .AppendFormat("Exception message: {0}\n", MessageException)
                .AppendFormat("--------------------------\n\n");

            // To database
            log4net.ThreadContext.Properties["RequestUri"] = RequestUri;
            log4net.ThreadContext.Properties["Username"] = Username;
            log4net.ThreadContext.Properties["Headers"] = Headers;
            log4net.ThreadContext.Properties["Body"] = Body;
            log4net.ThreadContext.Properties["StatusCode"] = StatusCode;
            log4net.ThreadContext.Properties["QueryString"] = QueryString;
            log4net.ThreadContext.Properties["HttpVerb"] = HttpVerb;

            _logger.LogError(logException.ToString());
        }
    }
}
