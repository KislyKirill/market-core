﻿using Market.Models;
using Market.Models.Entities;
using Microsoft.AspNetCore.Mvc;

namespace Market.WEB.Components
{
    public class CartSummaryViewComponent : ViewComponent
    {
        private Cart cart;

        public CartSummaryViewComponent(Cart cartService)
        {
            cart = cartService;
        }

        public IViewComponentResult Invoke()
        {
            return View(cart);
        }
    }
}
