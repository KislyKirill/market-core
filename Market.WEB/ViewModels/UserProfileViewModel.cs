﻿using Market.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Market.WEB.ViewModels
{
    public class UserProfileViewModel
    {
        public string Id { get; set; }

        [Display(Name = "Имя")]
        public string FirstName { get; set; }

        [Display(Name = "Фамилия")]
        public string LastName { get; set; }

        [Display(Name = "Email")]
        public string Email { get; set; }

        [Display(Name = "Год рождения")]
        public DateTime RegistrationDate { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Пароль")]
        public string Password { get; set; }

        [Compare("Password", ErrorMessage = "Пароли не совпадают")]
        [DataType(DataType.Password)]
        [Display(Name = "Подтвердить пароль")]
        public string PasswordConfirm { get; set; }

        public int FileModelId { get; set; }
        public string FileModelPath { get; set; }
        public string FileModelName { get; set; }

        public virtual FileModel FileModel { get; set; }
    }
}
