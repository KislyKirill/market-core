﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Market.WEB.ViewModels
{
    public class FileUploadViewModel
    {
        public string ErrorMessage { get; set; }
        public decimal filesize { get; set; }

        public string UploadUserFileValidation(IFormFile file)
        {
            try
            {
                var supportedTypes = new[] { "jpg", "jpeg", "png" };
                //var supportedTypes = new[] { "txt", "doc", "docx", "pdf", "xls", "xlsx" };
                var fileExt = System.IO.Path.GetExtension(file.FileName).Substring(1);
                if (!supportedTypes.Contains(fileExt))
                {
                    ErrorMessage = "File extension is invalid - only upload jpg/jpeg/png file";
                    return ErrorMessage;
                }
                else if (file.Length > (filesize * 1048576))
                {
                    ErrorMessage = "File size should be up to " + filesize + " MB";
                    return ErrorMessage;
                }
                else
                {
                    // File is successfully
                    return null;
                }
            }
            catch (Exception ex)
            {
                ErrorMessage = "Upload Container Should Not Be Empty or Contact Admin";
                return ErrorMessage;
            }
        }
    }
}
