﻿using Market.Models.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Market.WEB.ViewModels
{
    public class OrderViewModel
    {
        [Display(Name ="Номер заказа")]
        public string OrderId { get; set; }

        [DataType(DataType.Date)]
        [Display(Name ="Дата заказа")]
        public DateTime PurchaseDate { get; set; }

        public Cart Cart { get; set; }
        public User User { get; set; }
        public string UserId { get; set; }
        public virtual ICollection<CartLine> Lines { get; set; }
    }
}
