﻿using Market.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Market.WEB.ViewModels
{
    public class ProductViewModel
    {
        public int ProductId { get; set; }

        [Display(Name = "Название")]
        public string ProductName { get; set; }

        [Display(Name = "Цена")]
        public int Price { get; set; }

        [Display(Name = "Описание")]
        public string Description { get; set; }

        [Display(Name = "Дата добавления")]
        public DateTime AddedDate { get; set; }

        [JsonIgnore]
        public ICollection<FileModel> Photos { get; set; }
    }
}
