﻿using System;
using System.Collections.Generic;

namespace Market.WEB.ViewModels
{
    public class OrderListViewModel
    {
        public IEnumerable<OrderViewModel> Orders { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public string UserData { get; set; }
    }
}
