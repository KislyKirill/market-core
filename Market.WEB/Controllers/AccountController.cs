﻿using System.Threading.Tasks;
using AutoMapper;
using Market.BLL.Infrastructure;
using Market.BLL.Interfaces;
using Market.Models.Entities;
using Market.WEB.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Web;
using System.Net;
using System.Text;
using Market.BLL.DTO;

namespace Market.WEB.Controllers
{
    public class AccountController : Controller
    {
        IUserService userService;

        public AccountController(IUserService serv)
        {
            userService = serv;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Register()
        {
            return View();
        }


        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Register(RegisterViewModel model)
        {
            if (ModelState.IsValid)
            {
                var mapper = new MapperConfiguration(cfg => cfg.CreateMap<RegisterViewModel, UserDTO>()
                    .ForMember(x => x.UserName, m => m.MapFrom(a => a.Email)))
                    .CreateMapper();
                var user = mapper.Map<RegisterViewModel, UserDTO>(model);

                var host = new StringBuilder();
                host.AppendFormat(HttpContext.Request.Scheme)
                    .AppendFormat("://")
                    .AppendFormat(HttpContext.Request.Host.ToString());

                ResultOperation resultOperation = await userService.Register(user, host.ToString());
                if (resultOperation.IsSuccess)
                    return Content(resultOperation.Message);
                else
                    ModelState.AddModelError("", resultOperation.Message);
            }

            return View(model);
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> ConfirmEmail(string userId, string code)
        {

            ResultOperation result = await userService.ConfirmEmail(userId, code);
            if (result.IsSuccess)
            {
                return RedirectToAction("Index", "Home");
            }
            else
            {
                return View("Error");
            }
        }

        [HttpGet]
        public IActionResult Login(string returnUrl = null)
        {
            return View(new LoginViewModel { ReturnUrl = returnUrl });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(LoginViewModel model)
        {
            if (ModelState.IsValid)
            {
                var mapper = new MapperConfiguration(cfg => cfg.CreateMap<LoginViewModel, UserDTO>()).CreateMapper();
                var user = mapper.Map<LoginViewModel, UserDTO>(model);

                ResultOperation resultOperation = await userService.Login(user);

                if (resultOperation.IsSuccess)
                    return RedirectToAction("Index", "Home");
                else
                    ModelState.AddModelError("", resultOperation.Message);
            }

            return View(model);
        }

        [HttpPost]
        [AllowAnonymous]
        public IActionResult SignInWithGoogle()
        {
            var authenticationProperties = userService.SignInWithGoogle(Url.Action(nameof(HandleExternalLogin)));

            return Challenge(authenticationProperties, "Google");
        }

        [AllowAnonymous]
        public async Task<IActionResult> HandleExternalLogin()
        {
            ResultOperation resultOperation = await userService.HandleExternalLogin();
            if (!resultOperation.IsSuccess)
            {
                ModelState.AddModelError("", resultOperation.Message);
                return View();
            }

            return RedirectToAction("Index", "Home");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> LogOut()
        {
            await userService.LogOut();
            return RedirectToAction("Index", "Home");
        }
    }
}