﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using AutoMapper;
using Market.BLL.DTO;
using Market.BLL.Infrastructure;
using Market.BLL.Interfaces;
using Market.Models.Entities;
using Market.WEB.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using OfficeOpenXml;
using OfficeOpenXml.Style;

namespace Market.WEB.Controllers
{
    public class OrderController : Controller
    {
        IOrderService _orderService;
        IReportService _reportService;
        IEmailService _emailService;
        private Cart cart;

        private readonly IHostingEnvironment _hostingEnvironment;

        public OrderController(IOrderService orderServ, Cart cartService, IReportService reportServ, IEmailService emailServ, IHostingEnvironment hostEnv)
        {
            _orderService = orderServ;
            cart = cartService;
            _reportService = reportServ;
            _emailService = emailServ;
            _hostingEnvironment = hostEnv;
        }

        [Authorize]
        public IActionResult Index()
        {
            string userId = HttpContext.User.FindFirst(ClaimTypes.NameIdentifier).Value;
            IEnumerable<Order> ordersModel = new List<Order>();

            if (User.IsInRole("admin"))
            {
                ordersModel = _orderService.GetOrders();
            }
            else if (User.IsInRole("user") || User.IsInRole("manager"))
            {
                ordersModel = _orderService.GetOrdersCurrentUser(userId);
            }

            var mapper = new MapperConfiguration(cfg => cfg.CreateMap<Order, OrderViewModel>()).CreateMapper();
            var orders = mapper.Map<IEnumerable<Order>, List<OrderViewModel>>(ordersModel);

            return View(orders);
        }

        public ViewResult Checkout() => View(new Order());

        [HttpPost]
        [Authorize]
        public IActionResult Checkout(Order order)
        {
            if (ModelState.IsValid)
            {
                string userId = HttpContext.User.FindFirst(ClaimTypes.NameIdentifier).Value;
                _orderService.MakeOrder(order, userId);
                return RedirectToAction(nameof(Completed));
            }
            else
            {
                return View(order);
            }
        }

        [Authorize(Roles = "admin")]
        public IActionResult Report(string userData, DateTime? fromDate, DateTime? toDate)
        {
            var ordersModel = _orderService.Report(userData, fromDate, toDate);

            var mapper = new MapperConfiguration(cfg => cfg.CreateMap<Order, OrderViewModel>()).CreateMapper();
            var orders = mapper.Map<IEnumerable<Order>, List<OrderViewModel>>(ordersModel);

            ViewData["userData"] = userData;
            ViewData["fromDate"] = fromDate;
            ViewData["toDate"] = toDate;

            return PartialView(orders);
        }


        public async Task<IActionResult> ExportExcel(string userData, DateTime? fromDate, DateTime? toDate)
        {
            var ordersModel = _orderService.Report(userData, fromDate, toDate);
            var mapper = new MapperConfiguration(cfg => cfg.CreateMap<Order, OrderViewModel>()).CreateMapper();
            var orders = mapper.Map<IEnumerable<Order>, List<OrderViewModel>>(ordersModel);
            await Task.Yield();

            OrderListViewModel orderList = new OrderListViewModel()
            {
                Orders = orders,
                UserData = userData,
                FromDate = fromDate,
                ToDate = toDate
            };

            mapper = new MapperConfiguration(cfg => cfg.CreateMap<OrderListViewModel, OrderListDTO>()).CreateMapper();
            var orderDto = mapper.Map<OrderListViewModel, OrderListDTO>(orderList);

            DateTime currentDate = DateTime.Now;
            var stream = _reportService.ExportExcel(orderDto, currentDate);

            string excelName = $"Report-{currentDate.ToString("yyyyMMddHHmmssfff")}.xlsx";
            return File(stream, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", excelName);
        }

        public async Task<IActionResult> SendExcelToMail(string userData, DateTime? fromDate, DateTime? toDate, string emailAddress)
        {
            var ordersModel = _orderService.Report(userData, fromDate, toDate);
            var mapper = new MapperConfiguration(cfg => cfg.CreateMap<Order, OrderViewModel>()).CreateMapper();
            var orders = mapper.Map<IEnumerable<Order>, List<OrderViewModel>>(ordersModel);

            if (!string.IsNullOrEmpty(emailAddress))
            {
                OrderListViewModel orderList = new OrderListViewModel()
                {
                    Orders = orders,
                    UserData = userData,
                    FromDate = fromDate,
                    ToDate = toDate
                };

                mapper = new MapperConfiguration(cfg => cfg.CreateMap<OrderListViewModel, OrderListDTO>()).CreateMapper();
                var orderDto = mapper.Map<OrderListViewModel, OrderListDTO>(orderList);

                DateTime currentDate = DateTime.Now;
                string sWebRootFolder = _hostingEnvironment.WebRootPath;
                string excelName = $"Report-{currentDate.ToString("yyyyMMddHHmmssfff")}.xlsx";

                var stream = _reportService.ExportExcel(orderDto, currentDate);

                using (var fs = new FileStream(Path.Combine(sWebRootFolder, excelName), FileMode.Create, FileAccess.Write))
                {
                    await stream.CopyToAsync(fs);
                }

                await _emailService.SendEmailAsync(emailAddress, $"Report {currentDate.ToString("yyyyMMddHHmmssfff")}", "Market Core site administration has sent you a report.", Path.Combine(sWebRootFolder, excelName));
            }

            return View("Index", orders);
        }

        public IActionResult SendReport()
        {
            return PartialView();
        }

        public ViewResult Completed()
        {
            cart.Clear();
            return View();
        }
    }
}