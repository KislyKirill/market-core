﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Market.BLL.Interfaces;
using Market.WEB.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Market.WEB.Controllers
{
    [Authorize(Roles = "admin")]
    public class RoleController : Controller
    {
        IRoleService roleService;
        IUserService userService;

        public RoleController(IRoleService roleServ, IUserService userServ)
        {
            roleService = roleServ;
            userService = userServ;
        }

        public IActionResult Index()
        {
            return View(roleService.GetRoles());
        }

        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Create(string name)
        {
            var result = await roleService.Create(name);
            if (result.IsSuccess)
            {
                return RedirectToAction("Index");
            }
            else
            {
                ModelState.AddModelError("", result.Message);
            }

            return View(name);
        }

        [HttpPost]
        public async Task<IActionResult> Delete(string id)
        {
            await roleService.Delete(id);

            return RedirectToAction("Index");
        }

        public IActionResult UserList()
        {
            return View(userService.GetUsers());
        }

        public async Task<IActionResult> Edit(string userId)
        {
            var user = await userService.GetUser(userId);

            if (user != null)
            {
                var userRoles = await userService.GetUserRoles(user);
                var allRoles = roleService.GetRoles().ToList();

                RoleViewModel model = new RoleViewModel
                {
                    UserId = user.Id,
                    UserEmail = user.Email,
                    UserRoles = userRoles,
                    AllRoles = allRoles
                };
                return View(model);
            }

            return NotFound();
        }

        [HttpPost]
        public async Task<IActionResult> Edit(string userId, List<string> roles)
        {
            var result = await roleService.Edit(userId, roles);
            if (result.IsSuccess)
                return RedirectToAction("UserList");
            else
                return NotFound();
        }
    }
}