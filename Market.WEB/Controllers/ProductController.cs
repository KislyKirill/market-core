﻿using System.Collections.Generic;
using AutoMapper;
using Market.BLL.Interfaces;
using Market.Models.Entities;
using Market.WEB.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Market.WEB.Controllers
{
    public class ProductController : Controller
    {
        private IProductService productService;
        private IUploadFileService uploadFileService;

        public ProductController(IProductService serv, IUploadFileService uploadServ)
        {
            productService = serv;
            uploadFileService = uploadServ;
        }

        public IActionResult Index()
        {
            IEnumerable<Product> productsModel = productService.GetProducts();

            var mapper = new MapperConfiguration(cfg => cfg.CreateMap<Product, ProductViewModel>()).CreateMapper();
            var products = mapper.Map<IEnumerable<Product>, List<ProductViewModel>>(productsModel);

            return View(products);
        }

        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [Authorize(Roles = "admin")]
        public IActionResult Create(ProductViewModel productModel, IFormFileCollection uploads)
        {
            if (uploads.Count >= 1 && uploads.Count <= 4)
            {
                FileUploadViewModel fs = new FileUploadViewModel();
                fs.filesize = 2; // File size up to 2 MB
                foreach (var pic in uploads)
                {
                    string us = fs.UploadUserFileValidation(pic);
                    if (us != null)
                        ModelState.AddModelError(string.Empty, fs.ErrorMessage);
                }

                if (ModelState.IsValid)
                {
                    var mapper = new MapperConfiguration(cfg => cfg.CreateMap<ProductViewModel, Product>()).CreateMapper();
                    var product = mapper.Map<ProductViewModel, Product>(productModel);
                    uploadFileService.AddFileProduct(uploads, product);

                    productService.Create(product);

                    return RedirectToAction("Index");

                }
            }
            else
            {
                ModelState.AddModelError(string.Empty, "Add 1 to 4 pictures");
            }

            return View(productModel);
        }

        [Authorize(Roles = "admin")]
        public IActionResult Delete(int id)
        {
            productService.Delete(id);

            return RedirectToAction("Index");
        }

        [Authorize(Roles = "admin, manager")]
        public IActionResult Edit(int? id)
        {
            var product = productService.GetProduct(id);
            if (product != null)
            {
                var mapper = new MapperConfiguration(cfg => cfg.CreateMap<Product, ProductViewModel>()).CreateMapper();
                var productViewModel = mapper.Map<Product, ProductViewModel>(product);

                return View(productViewModel);
            }
            else
                return NotFound();
        }

        [HttpPost]
        [Authorize(Roles = "admin, manager")]
        public IActionResult Edit(ProductViewModel productModel)
        {
            if (ModelState.IsValid)
            {
                var mapper = new MapperConfiguration(cfg => cfg.CreateMap<ProductViewModel, Product>()).CreateMapper();
                var product = mapper.Map<ProductViewModel, Product>(productModel);

                productService.Update(product);

                return RedirectToAction("Index");
            }

            return View();
        }

        [Authorize(Roles = "admin, manager")]
        public IActionResult Details(int? id)
        {
            var product = productService.GetProduct(id);
            if (product != null)
            {
                var mapper = new MapperConfiguration(cfg => cfg.CreateMap<Product, ProductViewModel>()).CreateMapper();
                var productViewModel = mapper.Map<Product, ProductViewModel>(product);

                return View(productViewModel);
            }
            else
                return NotFound();
        }
    }
}