﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using AutoMapper;
using Market.BLL.DTO;
using Market.BLL.Interfaces;
using Market.Models.Entities;
using Market.WEB.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Market.WEB.Controllers
{
    public class UserController : Controller
    {
        private IUserService userService;
        private IUploadFileService fileService;
        private IUserProfileService userProfileService;

        public UserController(IUserService userServ, IUploadFileService fileServ, IUserProfileService userProfileServ)
        {
            userService = userServ;
            fileService = fileServ;
            userProfileService = userProfileServ;
        }

        [Authorize]
        public IActionResult Index()
        {
            var userId = HttpContext.User.FindFirst(ClaimTypes.NameIdentifier).Value;
            //var user = await userService.GetUser(userId);
            var userProfile = userProfileService.GetUserProfile(userId);

            var mapper = new MapperConfiguration(cfg => cfg.CreateMap<UserProfile, UserProfileViewModel>()).CreateMapper();
            var userViewModel = mapper.Map<UserProfile, UserProfileViewModel>(userProfile);

            if (userProfile.FileModelId != null)
            {
                var file = fileService.GetFile(userProfile.FileModelId);
                userViewModel.FileModelPath = file.Path;
                userViewModel.FileModelName = file.Name;
            }

            return View(userViewModel);
        }

        [Authorize]
        public IActionResult Edit()
        {
            var userId = HttpContext.User.FindFirst(ClaimTypes.NameIdentifier).Value;
            //var user = await userService.GetUser(userId);
            var userProfile = userProfileService.GetUserProfile(userId);

            var mapper = new MapperConfiguration(cfg => cfg.CreateMap<UserProfile, UserProfileViewModel>()).CreateMapper();
            var userViewModel = mapper.Map<UserProfile, UserProfileViewModel>(userProfile);

            if (userProfile.FileModelId != null)
            {
                var file = fileService.GetFile(userProfile.FileModelId);
                userViewModel.FileModelPath = file.Path;
                userViewModel.FileModelName = file.Name;
            }

            return View(userViewModel);
        }

        [HttpPost]
        public IActionResult Edit(UserProfileViewModel userProfileViewModel, IFormFile uploadedFile)
        {
            if (uploadedFile != null)
            {
                FileUploadViewModel fs = new FileUploadViewModel();
                fs.filesize = 2; // File size up to 2 MB

                string us = fs.UploadUserFileValidation(uploadedFile);
                if (us != null)
                    ModelState.AddModelError(string.Empty, fs.ErrorMessage);
            }

            if (ModelState.IsValid)
            {
                var mapper = new MapperConfiguration(cfg => cfg.CreateMap<UserProfileViewModel, UserProfile>()).CreateMapper();
                var userProfile = mapper.Map<UserProfileViewModel, UserProfile>(userProfileViewModel);

                string path = "/images/users/" + userProfile.Id;

                fileService.AddFileUser(uploadedFile, path, userProfile.Id, userProfile);
                userProfileService.Update(userProfile);

                return RedirectToAction("Index");
            }

            return View(userProfileViewModel);
        }
    }
}