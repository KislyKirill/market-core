﻿using System;
using System.Collections.Generic;
using System.Linq;
using Market.BLL.Interfaces;
using Market.Models.Entities;
using Market.WEB.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Market.WEB.Controllers
{
    public class CartController : Controller
    {
        private IProductService productService;
        private Cart cart;

        public CartController(IProductService serv, Cart cartService)
        {
            productService = serv;
            cart = cartService;
        }

        public ViewResult Index(string returnUrl)
        {
            return View(new CartIndexViewModel
            {
                Cart = cart,
                ReturnUrl = returnUrl
            });
        }

        [Authorize]
        public RedirectToActionResult AddToCart(int productId)
        {
            Product product = productService.GetProduct(productId);

            if (product != null)
            {
                cart.AddItem(product, 1);
            }
            return RedirectToAction("Index");
        }

        public RedirectToActionResult RemoveFromCart(int productId, string returnUrl)
        {
            Product product = productService.GetProduct(productId);

            if (product != null)
            {
                cart.RemoveLine(product);
            }
            return RedirectToAction("Index", new { returnUrl });
        }
    }
}