﻿using Market.BLL.Interfaces;
using Market.BLL.Services;
using Market.DAL.Context;
using Market.DAL.Interfaces;
using Market.DAL.Repositories;
using Market.Models;
using Market.Models.Entities;
using Market.WEB.Filters;
using Market.WEB.Logger;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using System.IO;

namespace Market.WEB
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<CookiePolicyOptions>(options =>
            {
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });

            //// Logging
            //services.AddLogging(builder =>
            //{
            //    builder.AddFilter("Microsoft", LogLevel.Critical);
            //    builder.AddFilter("System", LogLevel.Critical);
            //});

            // Connection context
            string connection = Configuration.GetConnectionString("MarketConnection");
            services.AddDbContext<MarketContext>(options => options.UseSqlServer(connection));

            // Identity 
            services.AddIdentity<User, IdentityRole>()
                .AddEntityFrameworkStores<MarketContext>()
                .AddDefaultTokenProviders();
            services.AddTransient<IUserService, UserService>();
            services.AddTransient<IRoleService, RoleService>();

            // Add our services
            services.AddTransient<IUnitOfWork, EFUnitOfWork>();
            services.AddTransient<IOrderService, OrderService>();
            services.AddTransient<IProductService, ProductService>();
            services.AddTransient<IReportService, ReportService>();
            services.AddTransient<IUploadFileService, UploadFileService>();
            services.AddTransient<IUserProfileService, UserProfileService>();

            // Cart
            services.AddScoped<Cart>(sp => SessionCart.GetCart(sp));
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();

            // Smtp server
            services.Configure<EmailSettings>(Configuration.GetSection("Smtp"));

            // send email 
            services.AddTransient<IEmailService, EmailService>();

            services.AddMvc(options =>
            {
                // Add Global filter
                options.Filters.Add(typeof(LoggerAttribute));
            }).SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

            services.AddCors(corsOptions =>
            {
                corsOptions.AddPolicy("fully permissive", configurePolicy => configurePolicy.AllowAnyHeader().AllowAnyMethod().AllowAnyOrigin().AllowCredentials());
            });
            services.AddAuthentication(options =>
            {
                options.DefaultSignOutScheme = IdentityConstants.ApplicationScheme;
            })
            .AddGoogle("Google", options =>
            {
                options.CallbackPath = new PathString("/google-callback");
                options.ClientId = "1023000861823-ioibskr7u7mv52aotqvlv0qo4ha0809k.apps.googleusercontent.com";
                options.ClientSecret = "ZgOjx-SQO9_39zsbSc-Kb1Yb";
            });


            services.AddMemoryCache();
            services.AddSession();
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddLog4Net();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseCookiePolicy();

            app.UseAuthentication();
            app.UseSession();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
